#!/usr/bin/python

import re
from dateutil import parser
import urllib

TIMEOUT = 5

id_pattern = re.compile('http://download.cnet.com/[^/]+/\d+-\d+_\d+-(\d+).html')
url_pattern = re.compile('^http://download.cnet.com/')

ENTRY_POINTS = [
    'http://download.cnet.com/',
    'http://download.cnet.com/windows/',
    'http://download.cnet.com/mac/',
    'http://download.cnet.com/ios/',
    'http://download.cnet.com/android/',
]

def normalize_url(url):
    link = re.sub('#.*', '', url)
    link = re.sub('.html?.*', '.html', url)
    return link.encode('utf-8')

meta_pattern = {
    'name': re.compile('productName="(.*?)"'),
    'developer_id': re.compile('<div class="publisherDescription.*?From <a href="/windows/[^/]+/\d+-\d+_\d+-(\d+).html">'),
    'category': re.compile('<a href="([^"]+)">[^<]+</a></li> <li id="softwareProductName"', re.DOTALL),
    'date_updated': re.compile('<span>Date added:</span>([^<]*?)</li>'),
    'size': re.compile('<span>File size:</span>([^<]*?)</li>', re.DOTALL),
    'version': re.compile('<span>Version:</span>([^<]*?)</li>', re.DOTALL),
    'os': re.compile('<span>Operating system:</span> <p class="OneLinkNoTx">([^<]*?)</p>', re.DOTALL),
    'price': re.compile('<span>Price:</span>([^<]*?)</li>'),
    'description': re.compile('<div class="publisherDescription.*?(<p>.*?)</div>', re.DOTALL),
}

platform_pattern = {
    'windows': re.compile('windows', re.IGNORECASE),
    'osx': re.compile('mac os|os x', re.IGNORECASE),
    'android': re.compile('android', re.IGNORECASE),
    'ios': re.compile('iphone|ios|i\s+9', re.IGNORECASE),
}

def extract_meta(html):
    meta = {}

    for key in meta_pattern.keys():
        pattern = meta_pattern[key]
        match = pattern.search(html)
        if match:
            meta[key] = match.group(1).strip()
        else:
            meta[key] = ''

    meta['languages'] = ''
    meta['cover_image'] = ''
    meta['app_rating_count'] = 0
    meta['app_rating_score'] = 0.0
    meta['content_rating'] = ''
    try:
        meta['date_updated'] = parser.parse(meta['date_updated']).strftime('%Y-%m-%d')
    except ValueError:
        meta['date_updated'] = '1970-01-01'
    match = re.search('(\d[\d|\.|,]+)', meta['price'])
    if match:
        meta['price'] = match.group(1)
        meta['price'] = re.sub('[^\d|^\.]', '', meta['price'])
    else:
        meta['price'] = 0.0
    meta['platform'] = 'unknown'
    for key in platform_pattern.keys():
        pattern = platform_pattern[key]
        match = pattern.search(meta['os'])
        if match:
            meta['platform'] = key
            break
    meta['os'] = re.sub('\s+', ' ', meta['os'])
    meta['category'] = re.sub('/[^/]+.html', '/', meta['category'])
    return meta
