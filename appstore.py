#!/usr/bin/python

import re
from dateutil import parser

TIMEOUT = 3

id_pattern = re.compile('https://itunes.apple.com/us/app/.*/id(\d+)')
url_pattern = re.compile('^https://itunes.apple.com/us/(app/|genre/ios).*')

ENTRY_POINTS = [
    'https://itunes.apple.com/us/genre/ios/id36',
    'https://itunes.apple.com/us/genre/ios-books/id6018',
    'https://itunes.apple.com/us/genre/ios-business/id6000',
    'https://itunes.apple.com/us/genre/ios-catalogs/id6022',
    'https://itunes.apple.com/us/genre/ios-education/id6017',
    'https://itunes.apple.com/us/genre/ios-entertainment/id6016',
    'https://itunes.apple.com/us/genre/ios-finance/id6015',
    'https://itunes.apple.com/us/genre/ios-food-drink/id6023',
    'https://itunes.apple.com/us/genre/ios-games/id6014',
    'https://itunes.apple.com/us/genre/ios-health-fitness/id6013',
    'https://itunes.apple.com/us/genre/ios-lifestyle/id6012',
    'https://itunes.apple.com/us/genre/ios-medical/id6020',
    'https://itunes.apple.com/us/genre/ios-music/id6011',
    'https://itunes.apple.com/us/genre/ios-navigation/id6010',
    'https://itunes.apple.com/us/genre/ios-news/id6009',
    'https://itunes.apple.com/us/genre/ios-newsstand/id6021',
    'https://itunes.apple.com/us/genre/ios-photo-video/id6008',
    'https://itunes.apple.com/us/genre/ios-productivity/id6007',
    'https://itunes.apple.com/us/genre/ios-reference/id6006',
    'https://itunes.apple.com/us/genre/ios-social-networking/id6005',
    'https://itunes.apple.com/us/genre/ios-sports/id6004',
    'https://itunes.apple.com/us/genre/ios-travel/id6003',
    'https://itunes.apple.com/us/genre/ios-utilities/id6002',
    'https://itunes.apple.com/us/genre/ios-weather/id6001'
]

def normalize_url(url):
    link = re.sub('#.*', '', url)
    link = re.sub('\?.*', '', url)
    return link.encode('utf-8')

meta_pattern = {
    'name': re.compile('<meta content="([^<]*?)" property="og:title" />'),
    'developer_id': re.compile('<a href="https://itunes.apple.com/us/artist/.*?/id(\d+).*?" class="view-more">View More By This Developer</a>'),
    'app_rating_score': re.compile("<div class='rating' role='img' tabindex='-1' aria-label='([^<]*?)stars,"),
    'app_rating_count': re.compile("<div class='rating' role='img' tabindex='-1' aria-label='.*?stars, (\d+)? Ratings'>"),
    'category': re.compile('<span class="label">Category: </span><a href="https://itunes.apple.com/us/genre/(.*?/id\d+)'),
    'release_date': re.compile('<li class="release-date"><span class="label">Released: </span>([^<]*?)</li>'),
    'date_updated': re.compile('<span class="label">Updated: </span>([^<]*?)<'),
    'size': re.compile('<span class="label">Size: </span>([^<]*?)<'),
    'version': re.compile('<span class="label">Version: </span>([^<]*?)<'),
    'os': re.compile('<span class="app-requirements">Compatibility: </span>([^<]*?)</p>'),
    'content_rating': re.compile('<div class="app-rating"><a href="https://itunes.apple.com/WebObjects/MZStore.woa/wa/appRatings">([^<]*?)</a>'),
    'price': re.compile('<div class="price">\$?([^<]*?)</div>'),
    'description': re.compile('<div more-text="More" metrics-loc="Titledbox_Description" class="product-review">.*?<p>(.*?)</p>', re.DOTALL),
    'languages': re.compile('<span class="label">Languages: </span>([^<]*?)<'),
    'cover_image': re.compile('<div id="left-stack">.*?<img.*?src="([^"]+)"', re.DOTALL),
}

def extract_meta(html):
    meta = {'platform': 'ios'}

    for key in meta_pattern.keys():
        pattern = meta_pattern[key]
        match = pattern.search(html)
        if match:
            meta[key] = match.group(1).strip()
        else:
            meta[key] = ''

    meta['app_rating_score'] = meta['app_rating_score'].replace(' and a half', '.5')
    if meta['app_rating_score'] == '':
        meta['app_rating_score'] = 0
    if meta['app_rating_count'] == '':
        meta['app_rating_count'] = 0

    # some Apps only have 'date released"
    try:
        meta['date_updated'] = parser.parse(meta['date_updated']).strftime('%Y-%m-%d')
    except ValueError:
        try:
            meta['date_updated'] = parser.parse(meta['release_date']).strftime('%Y-%m-%d')
        except ValueError:
            meta['date_updated'] = '1970-01-01'

    meta['price'] = re.sub('[^\d|^\.]', '', meta['price'])
    if meta['price'] == '':
        meta['price'] = 0

    return meta
