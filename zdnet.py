#!/usr/bin/python

import re
import time
import urllib

TIMEOUT = 3

id_pattern = re.compile('http://downloads.zdnet.com/product/([^/]+)/')
url_pattern = re.compile('^http://downloads.zdnet.com/(category|product)/.*')

ENTRY_POINTS = [
    'http://downloads.zdnet.com/price/all/',
    'http://downloads.zdnet.com/price/free/',
    'http://downloads.zdnet.com/price/freetotry/',
    'http://downloads.zdnet.com/price/purchase/',
    'http://downloads.zdnet.com/price/update/',
    'http://downloads.zdnet.com/category/2137/',
    'http://downloads.zdnet.com/category/2010/',
    'http://downloads.zdnet.com/category/2152/',
    'http://downloads.zdnet.com/category/18486/',
    'http://downloads.zdnet.com/category/2204/',
    'http://downloads.zdnet.com/category/2014/',
    'http://downloads.zdnet.com/category/13572/',
    'http://downloads.zdnet.com/category/31709/',
    'http://downloads.zdnet.com/category/35348/',
    'http://downloads.zdnet.com/category/2012/',
    'http://downloads.zdnet.com/category/2016/',
    'http://downloads.zdnet.com/category/2015/',
    'http://downloads.zdnet.com/category/2184/',
    'http://downloads.zdnet.com/category/18544/',
    'http://downloads.zdnet.com/category/2025/',
    'http://downloads.zdnet.com/category/2027/',
    'http://downloads.zdnet.com/category/13571/',
    'http://downloads.zdnet.com/category/2019/',
    'http://downloads.zdnet.com/category/2023/',
    'http://downloads.zdnet.com/category/32471/',
    'http://downloads.zdnet.com/category/2007/',
    'http://downloads.zdnet.com/category/20419/',
    'http://downloads.zdnet.com/category/2018/',
    'http://downloads.zdnet.com/category/13570/',
]

def normalize_url(url):
    link = re.sub('#.*', '', url)
    link = re.sub('\?mode=rss', '', link)
    link = re.sub('\?d=1', '', link)
    return link.encode('utf-8')

meta_pattern = {
    'name': re.compile('<Attribute name="title" value="([^<]*?)"/>'),
    'developer_id': re.compile('<Attribute name="author" value="([^<]*?)"/>'),
    'app_rating_score': re.compile('<Attribute name="rating" value="([^<]*?)"/>'),
    'category': re.compile('.*/category/(\d+)/'),
    'date_updated': re.compile('<Attribute name="published_timestamp" value="(\d+)"/>'),
    'size': re.compile('<th>File Size</th>.*?<td>([^<]*?)</td>', re.DOTALL),
    'version': re.compile('<th>Version</th>.*?<td>([^<]*?)</td>', re.DOTALL),
    'os': re.compile('<th>Operating System</th>.*?<td>([^<]*?)</td>', re.DOTALL),
    'price': re.compile('<Attribute name="price" value="\$?([^<]*?)"/>'),
    'description': re.compile('<div class="storyBody">([^<]*?)</div>', re.DOTALL),
}

platform_pattern = {
    'windows': re.compile('windows', re.IGNORECASE),
    'osx': re.compile('mac os|os x', re.IGNORECASE),
    'linux': re.compile('linux', re.IGNORECASE),
    'android': re.compile('android', re.IGNORECASE),
    'ios': re.compile('iphone|ios|i\s+9', re.IGNORECASE),
    'pocketpc': re.compile('pocket pc', re.IGNORECASE),
    'symbian': re.compile('symbian', re.IGNORECASE),
    'blackberry': re.compile('blackBerry', re.IGNORECASE),
    'palm': re.compile('palm', re.IGNORECASE),
    'java': re.compile('java', re.IGNORECASE),
    'xbox': re.compile('xbox', re.IGNORECASE),
}

def extract_meta(html):
    meta = {}

    for key in meta_pattern.keys():
        pattern = meta_pattern[key]
        match = pattern.search(html)
        if match:
            meta[key] = match.group(1).strip()
        else:
            meta[key] = ''

    meta['languages'] = ''
    meta['cover_image'] = ''
    meta['app_rating_count'] = 0
    if meta['app_rating_score'] is None or meta['app_rating_score'] == '':
        meta['app_rating_score'] = 0.0
    meta['content_rating'] = ''
    try:
        meta['date_updated'] = time.strftime('%Y-%m-%d', time.gmtime(float(meta['date_updated'])))
    except ValueError:
        meta['date_updated'] = '1970-01-01'
    try:
        meta['developer_id'] = urllib.unquote(meta['developer_id']).decode('utf-8-sig')
    except:
        meta['developer_id'] = urllib.unquote(meta['developer_id'])
    meta['platform'] = 'unknown'
    for key in platform_pattern.keys():
        pattern = platform_pattern[key]
        match = pattern.search(meta['os'])
        if match:
            meta['platform'] = key
            break
    meta['os'] = re.sub('\s+', ' ', meta['os'])
    meta['price'] = re.sub('[^\d|^\.]', '', meta['price'])
    if meta['price'] == '':
        meta['price'] = '0.0'
    return meta
