#!/usr/bin/python

import re
from dateutil import parser
import urllib

TIMEOUT = 5

id_pattern = re.compile('https://play.google.com/store/apps/details\?id=([^&]+)')
url_pattern = re.compile('^https://play.google.com/store/apps/.*')

ENTRY_POINTS = [
    'https://play.google.com/store/apps',
    'https://play.google.com/store/apps/category/BOOKS_AND_REFERENCE',
    'https://play.google.com/store/apps/category/BUSINESS',
    'https://play.google.com/store/apps/category/COMICS',
    'https://play.google.com/store/apps/category/COMMUNICATION',
    'https://play.google.com/store/apps/category/EDUCATION',
    'https://play.google.com/store/apps/category/ENTERTAINMENT',
    'https://play.google.com/store/apps/category/FINANCE',
    'https://play.google.com/store/apps/category/HEALTH_AND_FITNESS',
    'https://play.google.com/store/apps/category/LIBRARIES_AND_DEMO',
    'https://play.google.com/store/apps/category/LIFESTYLE',
    'https://play.google.com/store/apps/category/APP_WALLPAPER',
    'https://play.google.com/store/apps/category/MEDIA_AND_VIDEO',
    'https://play.google.com/store/apps/category/MEDICAL',
    'https://play.google.com/store/apps/category/MUSIC_AND_AUDIO',
    'https://play.google.com/store/apps/category/NEWS_AND_MAGAZINES',
    'https://play.google.com/store/apps/category/PERSONALIZATION',
    'https://play.google.com/store/apps/category/PHOTOGRAPHY',
    'https://play.google.com/store/apps/category/PRODUCTIVITY',
    'https://play.google.com/store/apps/category/SHOPPING',
    'https://play.google.com/store/apps/category/SOCIAL',
    'https://play.google.com/store/apps/category/SPORTS',
    'https://play.google.com/store/apps/category/TOOLS',
    'https://play.google.com/store/apps/category/TRANSPORTATION',
    'https://play.google.com/store/apps/category/TRAVEL_AND_LOCAL',
    'https://play.google.com/store/apps/category/WEATHER',
    'https://play.google.com/store/apps/category/APP_WIDGETS',
    'https://play.google.com/store/apps/category/GAME',
    'https://play.google.com/store/apps/category/ARCADE',
    'https://play.google.com/store/apps/category/BRAIN',
    'https://play.google.com/store/apps/category/CARDS',
    'https://play.google.com/store/apps/category/CASUAL',
    'https://play.google.com/store/apps/category/GAME_WALLPAPER',
    'https://play.google.com/store/apps/category/RACING',
    'https://play.google.com/store/apps/category/SPORTS_GAMES',
    'https://play.google.com/store/apps/category/GAME_WIDGETS'
]

def normalize_url(url):
    link = re.sub('#.*', '', url)
    link = re.sub('&reviewId=[^&]*', '', link)
    return link.encode('utf-8')

meta_pattern = {
    'name': re.compile('<div class="document-title" itemprop="name"> <div>([^<]*?)</div>'),
    'developer_id': re.compile('<a class="document-subtitle primary" href="/store/apps/developer\?id=([^<]*?)">'),
    'app_rating_score': re.compile('<meta content="([^<]*?)" itemprop="ratingValue">'),
    'app_rating_count': re.compile('<meta content="([^<]*?)" itemprop="ratingCount">'),
    'category': re.compile('<a class="document-subtitle category" href="/store/apps/category/([^<]*?)">'),
    'date_updated': re.compile('<div class="meta-info"> <div class="title">Updated</div> <div class="content" itemprop="datePublished">([^<]*?)</div> </div>'),
    'size': re.compile('<div class="meta-info"> <div class="title">Size</div> <div class="content" itemprop="fileSize">([^<]*?)</div> </div>'),
    'version': re.compile('<div class="meta-info"> <div class="title">Current Version</div> <div class="content" itemprop="softwareVersion">([^<]*?)</div> </div>'),
    'os': re.compile('<div class="meta-info"> <div class="title">Requires Android</div> <div class="content" itemprop="operatingSystems">([^<]*?)</div> </div>'),
    'content_rating': re.compile('<div class="meta-info"> <div class="title">Content Rating</div> <div class="content" itemprop="contentRating">([^<]*?)</div> </div>'),
    'price': re.compile('<meta content="[\$]?([^<]*?)" itemprop="price">'),
    'description': re.compile('<div class="id-app-orig-desc">([^<]*?)</div>'),
    'cover_image': re.compile('<div class="details-info">.*?<img class="cover-image" src="([^"]+)"', re.DOTALL),
}

def extract_meta(html):
    meta = {'platform': 'android'}

    for key in meta_pattern.keys():
        pattern = meta_pattern[key]
        match = pattern.search(html)
        if match:
            meta[key] = match.group(1).strip()
        else:
            meta[key] = ''

    meta['languages'] = ''
    try:
        meta['date_updated'] = parser.parse(meta['date_updated']).strftime('%Y-%m-%d')
    except ValueError:
        meta['date_updated'] = '1970-01-01'
    try:
        meta['developer_id'] = urllib.unquote(meta['developer_id']).decode('utf-8-sig')
    except:
        meta['developer_id'] = urllib.unquote(meta['developer_id'])
    meta['price'] = re.sub('[^\d|^\.]', '', meta['price'])
    return meta
