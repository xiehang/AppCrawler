#!/usr/bin/python

import re
import redis
import hashlib
import urlparse
import HTMLParser
import collections
import MySQLdb
import random
import time
import requests
import bz2

# HTTP client related
USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko'

class LinkParser(HTMLParser.HTMLParser):
    base = ''
    links = []
    def __init__(self, base_url):
        self.base = base_url

    def handle_starttag(self, tag, attrs):
        if (tag == 'a'):
            for attr,value in attrs:
                if (attr == 'href'):
                    self.links.append(urlparse.urljoin(self.base, value))
                    break

STORE_NAME = None
redis_client = None
mysql_client = None
REDIS_PARTITIONS = collections.deque(["%02x" % i for i in range(256)])
RAW_HTML_DIR = None

# redis related
REDIS_HOST = None
REDIS_PORT = None
REDIS_PASS = None
REDIS_DB = None

# mysql related
MYSQL_HOST = None
MYSQL_PORT = None
MYSQL_USER = None
MYSQL_PASS = None
MYSQL_DB = None

# TTL for different URLs
URL_TTL = None
RETRY_TTL = 3600

def init():
    global redis_client
    global mysql_client
    global STORE_NAME
    global REDIS_URL_SET_KEY
    global REDIS_SHUTDOWN_KEY

    REDIS_URL_SET_KEY = STORE_NAME + '-crawler-url-set'
    REDIS_SHUTDOWN_KEY = 'shutdown-' + STORE_NAME + '-crawler'

    redis_client = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB, password=REDIS_PASS)
    mysql_client = MySQLdb.connect(host=MYSQL_HOST, user=MYSQL_USER, passwd=MYSQL_PASS, db=MYSQL_DB, charset='utf8')

def need_to_shutdown():
    global redis_client

    return redis_client.get(REDIS_SHUTDOWN_KEY)

def url_to_key(url):
    hash = hashlib.md5()
    hash.update(url)
    return '%s-%s' % (STORE_NAME, hash.hexdigest())

def get_a_url():
    global redis_client
    global REDIS_PARTITIONS

    REDIS_PARTITIONS.rotate(random.choice(range(len(REDIS_PARTITIONS))))
    for partition in REDIS_PARTITIONS:
        url = redis_client.spop(REDIS_URL_SET_KEY + '-' + partition)
        if url:
            return url.encode('utf-8')

    # pull some URL from database for next request
    # we will query DB every 10 crawler requests of querying DB
    key = '%s-db-query-lock' % STORE_NAME
    redis_client.incr(key)
    old_value = int(redis_client.getset(key, '0'))
    if old_value < 10:
        redis_client.incr(key, old_value)
        return None

    cutoff_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time() - URL_TTL * 86400))
    cursor = mysql_client.cursor()
    cursor.execute("""
        SELECT   url
        FROM     apps
        WHERE    source = %s
        AND      date_crawled < %s
        LIMIT    4096
    """, (STORE_NAME, cutoff_time))
    url_list = cursor.fetchall()
    cursor.close()
    for url in url_list:
        add_a_url(url[0])

    # force to set to zero as we just finished DB query
    redis_client.set(key, 0)

    return None

def add_a_url(url):
    global redis_client

    hash = hashlib.md5()
    hash.update(url)
    redis_client.sadd(REDIS_URL_SET_KEY + '-' + hash.hexdigest()[0:2], url)

def get_url_ttl(url):
    global redis_client
    return redis_client.ttl(url_to_key(url))

def mark_as_fetched(url):
    global redis_client
    redis_client.setex(url_to_key(url), url, URL_TTL * 86400)

def mark_as_failed(url):
    global redis_client
    redis_client.setex(url_to_key(url), url, RETRY_TTL)

def mark_as_not_fetched(url):
    global redis_client
    redis_client.delete(url_to_key(url))

def mark_as_not_exist(url):
    global redis_client
    global mysql_client

    redis_client.setex(url_to_key(url), url, URL_TTL * 86400)

    cursor = mysql_client.cursor()
    cursor.execute("DELETE FROM apps WHERE source = %s AND url = %s", (STORE_NAME, url))
    cursor.close()
    mysql_client.commit()

def save(id, url, attr, html):
    if id == '' or id == '0' or not attr['category']:
        mark_as_not_exist(url)
        return ''

    save_raw(url, html)
    if attr['cover_image']:
        attr['cover_ext'] = save_image(id, attr['cover_image'])
    else:
        attr['cover_ext'] = ''
    return save_meta(id, url, attr)

def save_meta(id, url, attr):
    global mysql_client

    # reformat description
    attr['description'] = re.sub('<.*?>', '', re.sub('<(br|p)>', '\n', attr['description']))

    try:
        cursor = mysql_client.cursor()
        cursor.execute("""
            REPLACE
            INTO    apps
                    (source, id, url, platform, name, developer_id, app_rating_score, app_rating_count, category, date_updated, size, version, os, content_rating, price, description, languages, cover_ext)
            VALUES  (    %s, %s,  %s,       %s,   %s,           %s,               %s,                %s,      %s,           %s,   %s,      %s, %s,             %s,    %s,          %s,        %s,        %s)
        """, (STORE_NAME, id, url, attr['platform'], attr['name'], attr['developer_id'], attr['app_rating_score'], attr['app_rating_count'], attr['category'], attr['date_updated'], attr['size'], attr['version'], attr['os'], attr['content_rating'], attr['price'], attr['description'], attr['languages'], attr['cover_ext']))

        cursor.close()
        mysql_client.commit()
    except Exception as e:
        return "MySQL Error [%s], raw data [%s]" % (e, attr)

    return ''

def save_image(id, image_url):
    http_response = requests.get(image_url, headers = {'User-Agent': USER_AGENT})
    if http_response.status_code != 200:
        return ''
    match = re.match('image/(.*)', http_response.headers['content-type'], re.IGNORECASE)
    if match:
        image_type = match.group(1)
        hash = hashlib.md5()
        hash.update(STORE_NAME + '-' + id)
        digest = hash.hexdigest()
        filename = '%s/%s/%s/%s.%s' % (RAW_HTML_DIR, digest[0:2], digest[2:4], digest, image_type)
        with open(filename, 'w') as image_file:
            image_file.write(http_response.content)
            image_file.close()
        return image_type

def save_raw(url, html):
    hash = hashlib.md5()
    hash.update(url)
    digest = hash.hexdigest()
    filename = '%s/%s/%s/%s.html.bz2' % (RAW_HTML_DIR, digest[0:2], digest[2:4], digest)

    with bz2.BZ2File(filename, 'w') as html_file:
        html_file.write(html.encode('utf-8'))
        html_file.close()
