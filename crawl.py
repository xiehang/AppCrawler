#!/usr/bin/python -W error

import sys
import signal
import os
import requests
import re
import time
import argparse
import importlib
import platform
import datastore
import random

def signal_handler(signum, frame):
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    signal.signal(signal.SIGTERM, signal.SIG_DFL)

    logger('NOTICE', 'got signal %d' % signum)
    # any signal leads to termination of the main process
    # use this exception so we can get some backtrace info
    raise KeyboardInterrupt

def signal_timeout(signum, frame):
    logger('WARNING', 'task took abnormally long, restarting')
    sys.exit(2)

def logger(level, message):
    if level != 'DEBUG' or datastore.DEBUG != 0:
        sys.stdout.write('%s %s %06d %s\n' % (level, time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()), os.getpid(), message))
        sys.stdout.flush()

def worker(store_name):
    if datastore.PROXY_HOST is not None and datastore.PROXY_PORT != 0:
        os.environ['HTTP_PROXY'] = 'http://%s:%d' % (datastore.PROXY_HOST, datastore.PROXY_PORT)
        os.environ['HTTPS_PROXY'] = 'http://%s:%d' % (datastore.PROXY_HOST, datastore.PROXY_PORT)
    store = importlib.import_module(store_name)
    datastore.init()

    logger('NOTICE', 'started')

    for entry_point in store.ENTRY_POINTS:
        datastore.add_a_url(entry_point)

    # if timeout happens we are going to stop current running and start over
    signal.signal(signal.SIGALRM, signal_timeout)

    max_execution = store.TIMEOUT*10
    while not datastore.need_to_shutdown():
        # get_a_url may take quite long due to potential DB operation, so we need to disable timer
        signal.alarm(0)
        url = datastore.get_a_url()
        if not url:
            # disable timer to avoid killing by mistake
            logger('NOTICE', 'is waiting for fetchable URL')
            time.sleep(random.choice(range(max_execution)))
            continue
        url = store.normalize_url(url)

        # if a single page does not finish in 10x timeout then we are going to restart
        signal.alarm(max_execution)
        start_time = time.time()

        ttl = datastore.get_url_ttl(url)
        if ttl and ttl > 0:
            logger('DEBUG', 'skipped [%s] TTL is [%d]' % (url, ttl))
            continue

        try:
            http_response = requests.get(url, headers = {'User-Agent': datastore.USER_AGENT})
        except requests.exceptions.RequestException:
            datastore.add_a_url(url)
            logger('WARNING', 'failed to fetch [%s]' % url)
            datastore.mark_as_failed(url)
            continue

        if http_response.status_code != 200:
            logger('NOTICE', 'got HTTP-%s on [%s]' % (http_response.status_code, url))
            if http_response.status_code in [403]:
                # 403 means not allowed, should try again
                time.sleep(1)
                datastore.add_a_url(url)
            elif http_response.status_code in [404]:
                # 404 means does not exist, should not retry in near future
                datastore.mark_as_not_exist(url)
            else:
                # for other HTTP errors just skip the URL
                datastore.mark_as_failed(url)
            continue
        html = http_response.content
        if http_response.encoding:
            html = html.decode(http_response.encoding)
        else:
            html = html.decode('utf-8-sig')

        match = store.id_pattern.search(url)
        if match:
            err_msg = datastore.save(match.group(1), url, store.extract_meta(html), html)
            if err_msg:
                logger('WARNING', 'save_meta failed on [%s] because of [%s]' % (url, err_msg))
                datastore.mark_as_failed(url)
                continue

        parser = datastore.LinkParser(http_response.url)
        parser.reset()
        parser.feed(html)

        for link in filter(lambda x: store.url_pattern.search(x), parser.links):
            link = store.normalize_url(link)
            ttl = datastore.get_url_ttl(link)
            if not ttl:
                datastore.add_a_url(link)

        datastore.mark_as_fetched(url)

        duration = time.time() - start_time
        logger('NOTICE', 'spent %0.4f sec on [%s]' % (duration, url))

        # determine if we need to restart since long-running process gives poor performance
        if duration > store.TIMEOUT:
            logger('NOTICE', 'suicide because of poor performance')
            sys.exit(1)

    logger('NOTICE', 'administrative shutdown')
    sys.exit(0)

# default values - have to do things this way since values had been set in datastore.py
datastore.REDIS_HOST = 'localhost'
datastore.REDIS_PORT = 6379
datastore.REDIS_PASS = ''
datastore.REDIS_db = 0

datastore.MYSQL_HOST = 'localhost'
datastore.MYSQL_PORT = 3306
datastore.MYSQL_USER = 'crawler'
datastore.MYSQL_PASS = 'crawler'
datastore.MYSQL_DB = 'crawler'

# in days
datastore.URL_TTL = 3

parser = argparse.ArgumentParser(description='crawl mobile app stores')
parser.add_argument('--source', dest='STORE_NAME', help='source of meta info')
parser.add_argument('--proxy-host', dest='PROXY_HOST', help='HTTP proxy host', default=None)
parser.add_argument('--proxy-port', dest='PROXY_PORT', help='HTTP proxy port', default=0, type=int)

parser.add_argument('--url-ttl', dest='URL_TTL', help='TTL of URLs in days', default=0, type=int)
parser.add_argument('--html-dir', dest='RAW_HTML_DIR', help='directory for holding raw HTML')

parser.add_argument('--redis-host', dest='REDIS_HOST', help='redis host')
parser.add_argument('--redis-port', dest='REDIS_PORT', help='redis port', type=int)
parser.add_argument('--redis-pass', dest='REDIS_PASS', help='redis password')
parser.add_argument('--redis-db', dest='REDIS_DB', help='redis database', type=int)

parser.add_argument('--mysql-host', dest='MYSQL_HOST', help='MySQL host')
parser.add_argument('--mysql-port', dest='MYSQL_PORT', help='MySQL port', type=int)
parser.add_argument('--mysql-user', dest='MYSQL_USER', help='MySQL user')
parser.add_argument('--mysql-pass', dest='MYSQL_PASS', help='MySQL password')
parser.add_argument('--mysql-db', dest='MYSQL_DB', help='MySQL database')

parser.add_argument('--log-dir', dest='LOG_DIR', help='log file directory', default='./logs')
parser.add_argument('--debug', dest='DEBUG', help='debug level', default=0, type=int)

parser.parse_args(namespace=datastore)
if datastore.STORE_NAME is None:
    print 'ERROR: need to specify source of meta info'
    parser.print_help()
    sys.exit(1)

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

while True:
    log_file = open('%s/%s-%s-%s.log' % (datastore.LOG_DIR, datastore.STORE_NAME, platform.node(), time.strftime('%Y-%m-%d', time.localtime())), "a")
    with log_file as sys.stdout, log_file as sys.stderr:
        child_pid = os.fork()
        if not child_pid:
            # note that child will exit anyway
            worker(datastore.STORE_NAME)

        # parent
        try:
            (pid, status) = os.wait()
            if status == 0:
                # both hi-byte and low-byte are both zero, means child was not terminated because of something wrong (killed, exception, etc)
                # child shutdown gracefully, don't need to restart
                break
        except KeyboardInterrupt:
            logger('NOTICE', 'Main process is about to exit, killing child [%d]' % child_pid)
            os.kill(child_pid, signal.SIGTERM)
            break

    log_file.close()
